<?php

namespace app\controllers;

use app\models\Comments;
use app\models\CreateForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Posts;
use app\models\ContactForm;
use app\models\RegForm;
use app\models\User;

class CommentController extends Controller
{
    public function actionCreate($id)
    {
        $model = new Comments();
        if ($model->load(Yii::$app->request->post())) {
            $model->date = date('Y-m-d H:i:s');
            $model->user_id = Yii::$app->user->id;
            $model->post_id = $id;
            $model->save();
            return $this->redirect(['site/view', 'id' => $model->post_id]);
        }
    }
}