<?php

namespace app\controllers;

use app\models\Comments;
use yii\data\Pagination;
use app\models\CreateForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Posts;
use app\models\ContactForm;
use app\models\RegForm;
use app\models\User;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //Yii::$app->session->addFlash('info', 'Test message');
        //Yii::$app->session->addFlash('danger', 'Test danger message');
        $provider = new ActiveDataProvider([
            'query' => Posts::find(),
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        $provider = $provider->getModels();
        //$posts = Posts::find();
        //$count = $posts->count();
        //$pages = new Pagination(['totalCount' => $count, 'pageSize' => 3]);
        //$pages->pageSizeParam = false;
        //$posts = $posts->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', [
                'provider' => $provider,
                //'pages' => $pages,
            ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionReg()
    {
        $model = new RegForm();
        if ($model->load(Yii::$app->request->post())) {
            $user = new User();
            $user->login = $model->login;
            $user->password = hash('ripemd128', $model->password1);
            if ($user->save() && $model->login()) {
                return $this->goHome();
            }
        }

        return $this->render('reg', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionCreate()
    {
        $model = new CreateForm();
        if ($model->load(Yii::$app->request->post()) && $model->addPost()) {
            Yii::$app->session->addFlash('info', 'Ваш пост успешно добавлен');
            return $this->goHome();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = Posts::findOne(['id' => $id]);
        $comments = $model->comments;
        if ($model->delete()) {
            foreach ($comments as $comment) {
                $comment->delete();
            }
            Yii::$app->session->addFlash('danger', 'Запись была удалена');
            return $this->goHome();
        }
    }

    public function actionEdit($id)
    {
        $model = Posts::findOne(['id' => $id]);
        if ($model->load(Yii::$app->request->post())) {
            $model->date = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->addFlash('info', 'Пост был успешно изменен');
                return $this->goHome();
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        if ($post = Posts::findOne(['id' => $id])) {
            $model= new Comments();
            /*if ($model->load(Yii::$app->request->post())) {
                $model->date = date('Y-m-d H:i:s');
                $model->user_id = Yii::$app->user->id;
                $model->post_id = $id;
                $model->save();
                return $this->refresh();
            }*/

            return $this->render('view', [
                'post' => $post,
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
