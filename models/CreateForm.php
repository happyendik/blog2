<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CreateForm extends Model
{
    public $title;
    public $text;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['title', 'text'], 'required'],
        ];
    }

    public function addPost() {
        $post = new Posts();
        $post->user_id = Yii::$app->user->id;
        $post->title = $this->title;
        $post->text = $this->text;
        $post-> date = date('Y-m-d H:i:s');
        $post->save();
        return $post->save();

    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
