<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$post = \app\models\Posts::findOne(['id' => $model->post_id]);

echo '<h3>' . $post->title . '</h3><br>';
echo $post->text . '<br>';
echo $post->date . '<br>';
echo '<hr>';

foreach ($post->comments as $comment) {
    echo $comment->text . '<br>';
    echo $comment->date . '<br>';
    echo '<hr>';
}
?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'text')->textInput() ?>


<div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

