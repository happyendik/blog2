<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'title')->textInput(['autofocus' => true]); ?>

<?= $form->field($model, 'text')->textInput(); ?>

<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>