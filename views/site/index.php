<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/*
foreach ($posts as $post) {
    ?>
    <div class="container">
        <h3><?= $post->title ?></h3>
        <p><?= $post->text ?></p>
        <a href="<?= Url::to(['site/view', 'id' => $post->id]) ?>">комментарии <span class="badge"><?=count($post->comments)?></span></a>
        <?php
        if (!Yii::$app->user->isGuest) {
            echo "<br><br><a class='btn btn-danger' href='".Url::to(['site/delete', 'id' => $post->id])."' role='button'>Удалить</a>  ";
            echo "<a class='btn btn-warning' href='".Url::to(['site/edit', 'id' => $post->id])."' role='button'>Редактировать</a>";
        }

        ?>
        <hr>
    </div>

    <?php
}
*/
//echo LinkPager::widget([
  //      'pagination' => $provider->pagination,
    //]);

echo ListView::widget([
    'provider' => $provider,
    'itemView' => '_post',
    'viewParams' => [
        'fullView' => true,
        'context' => 'main-page',
        // ...
    ],
]);
