<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = "Новость №{$post->id}";
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h3><?=$post->title?></h3>
    <br>
    <?=$post->text?>
    <br>
    <?=$post->date?>
    <hr>


    <?php
    echo '<b>комментарии к посту:<br></b>';
    foreach ($post->comments as $comment) {
    ?>
    <?=$comment->text?>
    <br>
    <?=$comment->date?>
    <br>
    автор поста:  <?=$comment->user->login?>
    <br>
    <br>
    <?php
    }

    if (!Yii::$app->user->isGuest) {

    $form = ActiveForm::begin([
            'action' => ['comment/create','id' => $post->id],
    ]);

    echo $form->field($model, 'text');
    ?>

    <div class="form-group">
    <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
    }
    ?>
</div>
